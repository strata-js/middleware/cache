# Strata Cache Middleware
Caching middleware for use with a [Strata Service](https://gitlab.com/stratajs/strata).
See the [Strata Middleware](https://gitlab.com/stratajs/strata/-/blob/master/docs/middleware.md) documentation for more details.

Strata Cache Middleware supports three types of built-in cache stores:
 1. memory cache (internally uses [node-cache](https://www.npmjs.com/package/node-cache))
 2. redis cache (internally uses [ioredis](https://www.npmjs.com/package/ioredis)) 
 3. null cache (you can turn cache off by configuration without any code changes)

**Custom Cache Stores are also supported as long as they implement the ICacheStore interface.

## Installation

This middleware package is published via NPM's npm repository.

```bash
// npm
$ npm add @strata-js/middleware-cache

// yarn
$ yarn add @strata-js/middleware-cache
```

## Usage

#### Using a built-in cache store

```typescript
import { Context, configMan } from '@strata-js/strata';
import { CacheMiddleware } from '@strata-js/middleware-cache';

const cacheMiddleware = new CacheMiddleware(configMan.get('cache'));

context.operation('doSomething', async(request) : Promise<any> =>
{
    return this._doSomething(request.payload);
}, [cacheMiddleware]);

// Config for using the built-in memory cache
export default {
    cache: {
        kind: 'memory',
        key: 'test-service'
    }
}

// Config for using the built-in redis cache
export default {
    cache: {
        kind: 'redis',
        key: 'test-service',
        options: {
            host: 'localhost',
            port: 6379,
            db: 1
        },
        ttl: 60 * 60 * 24
    }
}

// Config for using the built-in null cache (no cache)
export default {
    cache: {
        kind: 'none'
    }
}
```
#### Using a custom cache store

```typescript
import { Context, configMan } from '@strata-js/strata';
import { CacheMiddleware, ICacheStore } from '@strata-js/middleware-cache';

class CustomStore implements ICacheStore
{
    async get(key : string) : Promise<unknown>
    {
        console.log(`"Getting" value from cache for ${ key }`);
        return;
    } // end get

    async set(key : string, value : unknown) : Promise<unknown>
    {
        console.log(`"Setting" value in cache for ${ key }. Value is ${ JSON.stringify(value) }`);
        return;
    } // end set
}

const cacheMiddleware = new CacheMiddleware(new CustomStore(), { kind: 'custom', key: 'test-service' });

context.operation('doSomething', async(request) : Promise<any> =>
{
    return this._doSomething(request.payload);
}, [cacheMiddleware]);
```
