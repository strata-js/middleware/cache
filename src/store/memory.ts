// ---------------------------------------------------------------------------------------------------------------------
// In Memory Store
// ---------------------------------------------------------------------------------------------------------------------

import NodeCache from 'node-cache';

// Interfaces
import { ICacheStore } from './iCacheStore.js';

// ---------------------------------------------------------------------------------------------------------------------

export class MemoryStore implements ICacheStore
{
    private readonly _memoryCache;

    constructor(options ?: NodeCache.Options | undefined)
    {
        const defaultOptions : NodeCache.Options = {
            stdTTL: 60 * 60 * 24, // default to 24 hour ttl
        };
        this._memoryCache = new NodeCache(Object.assign(defaultOptions, options));
    }

    /**
     * Get a value from the cache.
     *
     * @param key - The key to get the value for.
     *
     * @returns The value from the cache.
     */
    async get<T>(key : string) : Promise<T>
    {
        return new Promise((resolve, reject) =>
        {
            try
            {
                const value = this._memoryCache.get(key);
                resolve(value);
            }
            catch (ex)
            {
                reject(ex);
            }
        });
    }

    async set(key : string, value : unknown) : Promise<unknown>
    {
        return new Promise((resolve, reject) =>
        {
            try
            {
                this._memoryCache.set(key, value);
                resolve(key);
            }
            catch (ex)
            {
                reject(ex);
            }
        });
    }
}

// ---------------------------------------------------------------------------------------------------------------------
