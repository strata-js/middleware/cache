// ---------------------------------------------------------------------------------------------------------------------
// Redis Store
// ---------------------------------------------------------------------------------------------------------------------

import { Redis, RedisOptions } from 'ioredis';
import { logging } from '@strata-js/strata';
import { ICacheStore } from './iCacheStore.js';

// ---------------------------------------------------------------------------------------------------------------------

const logger = logging.getLogger('Cache Middleware');

// ---------------------------------------------------------------------------------------------------------------------

export class RedisStore implements ICacheStore
{
    private readonly _redis : Redis;

    private _handleRedisConnectionEnd() : void
    {
        logger.warn('Redis connection ended. (Will no longer retry.)');
    }

    constructor(options : RedisOptions, private _ttl : number | undefined)
    {
        const maxTime = 15000;
        this._redis = new Redis({
            // Defaults
            maxRetriesPerRequest: null,
            keepAlive: 180000, // enable keepAlive and set the idleDelay to 3 minutes
            connectionName: 'Cache Middleware',

            // Config
            ...options,

            // Overrides
            retryStrategy(times)
            {
                // If it's a hiccup, reconnect immediately
                if(times <= 1)
                {
                    return 0;
                }

                // Make sure `delay` is less than `maxTime`.
                return Math.min((0.1 * (1.2 ^ times)) * 1000, maxTime);
            },
        });

        // Connect to redis events
        this._redis.on('ready', () =>
        {
            logger.debug(`[${ this._redis.options.connectionName }] Redis is connected and server reports ready.`);
        });

        this._redis.on('close', () =>
        {
            if(this._redis.status !== 'reconnecting')
            {
                logger.warn(`[${ this._redis.options.connectionName }] Redis connection closed.`);
            }
        });

        this._redis.on('end', this._handleRedisConnectionEnd);

        this._redis.on('error', (error : { syscall : string; message : unknown; }) =>
        {
            if(error.syscall === 'connect')
            {
                logger.error(`[${ this._redis.options.connectionName }] Redis connection error:`, error.message);
            }
            else
            {
                logger.error(`[${ this._redis.options.connectionName }] Redis error:`, error.message);
            }
        });
    }

    async get(key : string) : Promise<unknown>
    {
        const result = await this._redis.get(key);
        if(result)
        {
            return JSON.parse(result);
        }
        return result;
    }

    async set(key : string, value : unknown) : Promise<unknown>
    {
        return this._ttl
            ? this._redis.setex(key, this._ttl, JSON.stringify(value)) : this._redis.set(key, JSON.stringify(value));
    }

    async teardown() : Promise<void>
    {
        this._redis.off('end', this._handleRedisConnectionEnd);
        this._redis.disconnect();
    }
}

// ---------------------------------------------------------------------------------------------------------------------
