// ---------------------------------------------------------------------------------------------------------------------
// Cache Middleware
// ---------------------------------------------------------------------------------------------------------------------

import { RedisOptions } from 'ioredis';
import { Options as NodeCacheOptions } from 'node-cache';
import { XOR } from 'ts-essentials';
import { Request, logging, OperationMiddleware, BasicLogger } from '@strata-js/strata';

// Stores
import { ICacheStore, MemoryStore, NullStore, RedisStore } from './store/index.js';

// ---------------------------------------------------------------------------------------------------------------------

export interface RedisCacheMiddlewareConfiguration
{
    kind : 'redis';
    key ?: string;
    options : RedisOptions;
    ttl ?: number;
}

export interface MemoryCacheMiddlewareConfiguration
{
    kind : 'memory';
    key ?: string;
    options ?: NodeCacheOptions;
}

export interface DefaultCacheMiddlewareConfiguration
{
    kind : 'none';
}

export interface CustomCacheMiddlewareConfiguration
{
    kind : 'custom';
    key ?: string;
}

export type CacheMiddlewareConfiguration = XOR<DefaultCacheMiddlewareConfiguration,
    XOR<RedisCacheMiddlewareConfiguration, MemoryCacheMiddlewareConfiguration>>;

export class CacheMiddleware implements OperationMiddleware
{
    private readonly _logger : BasicLogger;
    private readonly _store : ICacheStore;
    private readonly _cacheConfiguration : CacheMiddlewareConfiguration | CustomCacheMiddlewareConfiguration;

    constructor(cacheStore : ICacheStore, cacheMiddlewareConfiguration : CustomCacheMiddlewareConfiguration)
    constructor(cacheMiddlewareConfiguration : CacheMiddlewareConfiguration)
    constructor(cacheOrStore : ICacheStore | CacheMiddlewareConfiguration, customConfig ?: CustomCacheMiddlewareConfiguration)
    {
        this._logger = logging.getLogger('cacheMiddleware');
        if('kind' in cacheOrStore) // param is a CacheMiddlewareConfiguration
        {
            this._cacheConfiguration = cacheOrStore;
            switch (this._cacheConfiguration.kind)
            {
                case 'redis':
                    this._store = new RedisStore(this._cacheConfiguration.options, this._cacheConfiguration.ttl);
                    break;
                case 'memory':
                    this._store = new MemoryStore(this._cacheConfiguration.options);
                    break;
                case 'none':
                    this._store = new NullStore();
                    break;
                default:
                    this._logger.warn('unsupported cacheKind. No cache is being used.');
                    this._store = new NullStore();
            }
        }
        else
        {
            this._store = cacheOrStore;
            this._cacheConfiguration = customConfig || { kind: 'custom' };
        }
    }

    public async beforeRequest(request : Request & { cacheHit ?: boolean })
        : Promise<(Request & { cacheHit ?: boolean }) | undefined>
    {
        request.cacheHit = false;
        const key = this._getCacheKey(request);
        const result = await this._store.get(key);
        if(result)
        {
            request.cacheHit = true;
            request.succeed(result);
        }
        return request as Request & { cacheHit : boolean };
    }

    public async success(request : Request & { cacheHit ?: boolean })
        : Promise<(Request & { cacheHit ?: boolean }) | undefined>
    {
        const key = this._getCacheKey(request);
        try
        {
            if(!request.cacheHit)
            {
                await this._store.set(key, request.response);
            }
        }
        catch (error)
        {
            const knownError = error as Error;
            this._logger.debug('Failed to set cache: ', knownError.message);
        }

        return request;
    }

    public async failure(request : Request) : Promise<Request | undefined>
    {
        return request;
    }

    public async teardown() : Promise<void>
    {
        if(typeof this._store.teardown === 'function')
        {
            await this._store.teardown();
        }
    }

    private _getCacheKey(request : Request) : string
    {
        const allKeys : string[] = [];
        JSON.stringify(request.payload, (key, value) => { allKeys.push(key); return value; });
        allKeys.sort();
        return [
            this._cacheConfiguration.key || 'cache',
            request.context,
            request.operation,
            JSON.stringify(request.payload, allKeys)
        ].join(':');
    }
}
// ---------------------------------------------------------------------------------------------------------------------
// Exports
// ---------------------------------------------------------------------------------------------------------------------

export { ICacheStore } from './store/index.js';

// ---------------------------------------------------------------------------------------------------------------------
